from tkinter import *
import random

class Board:

    def __init__(self, parent, sq_size, color):
        self.parent = parent   # parent is root
        self.sq_size = sq_size
        self.color = color
        self.unused_squares_dict = { '00': 1, '10': 2, '20': 3,'01': 4, '11': 5, '21': 6,'02': 7, '12': 8, '22': 9}
        self.container = Frame(self.parent)
        self.container.pack()
        self.canvas = Canvas(self.container,width= self.sq_size * 3,height= self.sq_size * 3)
        self.canvas.grid()

    def get_unused_squares_dict(self):
        return self.unused_squares_dict

    def reset_unused_squares_dict(self):
        self.unused_squares_dict = { '00': 1, '10': 2, '20': 3,'01': 4, '11': 5, '21': 6,'02': 7, '12': 8, '22': 9  }

    def draw_board(self):
        for row in range(3):
            for column in range(3):
                self.canvas.create_rectangle(self.sq_size  * column,self.sq_size  * row,self.sq_size  * (column + 1),self.sq_size  * (row + 1),fill = self.color)

    def get_row_col(self, evt):
        return evt.x, evt.y

    def floor_of_row_col(self, col, rw):
        """
        normalize col and row number for all board size by taking
        the floor of event's x and y coords as col and row, respectively
        """
        col_flr = col // self.sq_size
        rw_flr = rw // self.sq_size
        return col_flr, rw_flr

    def convert_to_key(self, col_floor, row_floor):
        # turn col and row's quotient into a string for the key
        return str(col_floor) + str(row_floor)

    def find_coords_of_selected_sq(self, evt):
        """
        finding coords in a 9-sq grid

        params: event triggered by user's click
        return: tuple of two values for second corner's col, row
        """
        # saves row and col tuple into two variables
        column, row = self.get_row_col(evt)
        column_floor, row_floor = self.floor_of_row_col(column, row)
        # convert to key, use key to locate position in 3x3 grid
        rowcol_key_str = self.convert_to_key(column_floor, row_floor)
        corner_column = (column_floor * self.sq_size) + self.sq_size
        corner_row =  (row_floor  * self.sq_size) + self.sq_size
        print("rowcol_key_str: " + str(rowcol_key_str))
        return corner_column, corner_row

class GameApp(object):

    def __init__(self, parent):
        self.parent = parent
        self.board = Board(self.parent, 100, "#ECECEC")
        self.board.draw_board()
        self.unused_squares_dict = self.board.get_unused_squares_dict()
        self.initialize_buttons()
        self.show_menu()

    def initialize_buttons(self):
        self.reset_button = Button(self.board.container, text = "RESET",width = 25,command = self.restart)

    def show_menu(self):
        self.reset_button.grid()

    def restart(self):
        self.board.container.destroy()
        self.board = Board(self.parent, 100, "#ECECEC")
        self.board.draw_board()
        self.initialize_buttons()
        self.show_menu()

    def add_to_player_sq(self, key, player_sq):
        """
        use key of col and row to locate position of square
        and add square to player's selected_sq set
        :param key: str concat of col and row key str
        """
        current_selected_sq = self.board.unused_squares_dict[key]
        print("current selected sq  ---->", current_selected_sq)
        print("BEFORE player selected_sq: ", player_sq)
        player_sq.add(current_selected_sq)   # player 1 = {1}
        print("AFTER player selected_sq: ", player_sq)

    def delete_used_sq(self, key):
        # delete selected sq in self.board.unused_squares_dict
        print(" ---- square to delete ---: ", self.board.unused_squares_dict[key])
        print("unused squares dictionary before: ", self.board.unused_squares_dict)
        del self.board.unused_squares_dict[key]
        print("unused squares dictionary after: ", self.board.unused_squares_dict)

def main():
    window = Tk()
    window.title("Tic Tac Toe")
    tictac_game = GameApp(window)  # root is parent
    window.mainloop()

if __name__ == '__main__':
    main()


