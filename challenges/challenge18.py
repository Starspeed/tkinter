from tkinter import *

class text(Frame):
    def __init__(self, Label):
        frame = Frame(height = 200, width = 200)
        self.label = Label(self, text='Welcome')
        self.label.pack(side='top')
        self.flash()

    def flash(self):
        self.label = Label(self, text = 'Test')
        self.after(50, self.flash)

window = Tk()
text(window).pack()
window.mainloop()
'''
from tkinter import *




class TkinterBlinker(Frame):

    def __init__(self, *args, **kwargs):
        Frame.__init__(self, *args, **kwargs)
        self.pack()
        self.label_visible = True
        self.label = Label(self, text='Welcome')
        self.label.pack()
        self.blinking_label()

    def blinking_label(self):
        if self.label_visible:
            self.label.pack()
        else:
            self.label.forget()

        self.label_visible = not self.label_visible
        self.after(50, self.blinking_label)

root = Tk()
frame = TkinterBlinker(root)
root.mainloop()

'''
