from tkinter import *

class TrafficLights:


    def __init__(self):
        window = Tk()
        frame = Frame(height=200, bd=200, relief = SUNKEN)
        frame.pack()
        self.color = StringVar()

        Oval = Radiobutton(frame, text="Oval", bg="grey", variable=self.color, value="O", command=self.RadioChange)
        Oval.grid(row=10, column=1)

        Rectangle = Radiobutton(frame, text="Rectangle", bg="grey", variable=self.color, value="R", command=self.RadioChange)
        Rectangle.grid(row = 10, column = 2)

        Fill = Radiobutton(frame, text="Filled", bg="grey", variable=self.color, value="F", command=self.RadioChange)
        Fill.grid(row = 10, column = 3)

        self.canvas = Canvas(window, width=450, height=300, bg="white")
        self.canvas.pack()

        self.oval = self.canvas.create_oval(10, 10, 110, 110, fill="white")
        self.rectangle = self.canvas.create_rectangle(50, 150, 250, 100, fill="white")
        self.oval_green = self.canvas.create_oval(230, 10, 330, 110, fill="white")

        self.color.set('O')
        self.canvas.itemconfig(self.rectangle)

        window.mainloop()
    def RadioChange(self):
        color = self.color.get()

        if color == 'O':
            self.canvas.itemconfig(self.oval, fill="white")
            self.canvas.delete(self.rectangle)
            self.canvas.delete(self.oval_green)
        elif color == 'R':
            self.canvas.delete(self.oval)
            self.canvas.itemconfig(self.rectangle, fill="white")
            self.canvas.delete(self.oval_green)
        elif color == 'F':
            self.canvas.itemconfig(self.oval, fill="grey")
            self.canvas.itemconfig(self.rectangle, fill="grey")
            self.canvas.itemconfig(self.oval_green, fill="grey")

TrafficLights()