from tkinter import *
import random
window = Tk()
canvas = Canvas(window, width = 500, height = 500)
canvas.pack()
def randomline():
        x = random.randint(0, 500)
        y = random.randint(0, 500)
        q = random.randint(0, 500)
        w = random.randint(0, 500)
        canvas.create_line(x, q, w, y, fill="red", dash=(4, 4))

button = Button(window, text = 'Draw a random line', command = randomline)
button.pack()
window.mainloop()